# Vault Demo for Kaeffchen

## Prerequisites

- Docker or Vagrant on your Machine

## Setting up your Vault Instance

### Docker


```sh
docker-compose up -d
```


### Vagrant

```
vagrant up
```


## First setting up your vault Client 

By Default Vault Client trys to connect via `HTTPS` so we need to change this in according way to `HTTP`

So export the `VAULT_ADDR`.

```sh
export VAULT_ADDR=http://127.0.0.1:8200
```


## Check your Vault Instance

```sh
vault status
Key                Value
---                -----
Seal Type          shamir
Initialized        false
Sealed             true
Total Shares       0
Threshold          0
Unseal Progress    0/0
Unseal Nonce       n/a
Version            n/a
HA Enabled         false
```

Now you need to initialize your Instance

For an easy way use http://127.0.0.1:8200
#!/bin/bash
set -e 
set -o pipefail

MKCERT_BIN=/usr/bin/mkcert
VAULT_VERSION=${1:-1.5.4}

DOCKER_IMAGE=solidnerd/vault:$VAULT_VERSION-self-signed

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mkdir -p $DIR/certs

cp "$($MKCERT_BIN -CAROOT)/rootCA.pem" $DIR/certs/mkcert.crt

docker image build --build-arg VAULT_VERSION=$VAULT_VERSION -t $DOCKER_IMAGE $DIR
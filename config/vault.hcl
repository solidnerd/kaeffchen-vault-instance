ui = true
listener "tcp" {
    telemtry {
        unauthenticated_metrics_access = "true"
    }
    address = "0.0.0.0:8200"
    # TLS Enabled
    # tls_cert_file = "/vault/config/vault.kaeffchen.demo.pem"
    # tls_key_file = "/vault/config/vault.kaeffchen.demo-key.pem"
    # Otherwise use
    tls_disable = 1
}

storage "file" {
    path = "/vault/file"
}


api_addr = "https://0.0.0.0:8200"
cluster_addr = "https://0.0.0.0:8201"